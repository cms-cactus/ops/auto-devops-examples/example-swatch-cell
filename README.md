[![pipeline status](https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples/makefile-full/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples/makefile-full/-/commits/master)

# AutoDevOps SWATCH Cell Example

This project is an example implementation of a [GitLab CI configuration](.gitlab-ci.yml) that leverages [AutoDevOps](https://gitlab.cern.ch/cms-cactus/ops/auto-devops/) 
templates compile a(n empty) SWATCH cell, as well as publishing the RPMs, making a docker image, and deploying RPMs to P5.

![pipeline example](docs/pipeline.png)

For additional documentation about the CI configuration, consult the [SWATCH Cell AutoDevOps preset documentation](https://gitlab.cern.ch/cms-cactus/ops/auto-devops/-/blob/master/presets/swatch-cell-xdaq15.md)