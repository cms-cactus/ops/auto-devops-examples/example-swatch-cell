BUILD_HOME:=$(shell pwd)
Project=cactusprojects
Packages= example/cell

include /opt/cactus/build-utils/mfCommonDefs.mk

include $(XDAQ_ROOT)/build/mfAutoconf.rules
include $(XDAQ_ROOT)/build/mfDefs.$(XDAQ_OS)

include $(XDAQ_ROOT)/build/Makefile.rules
include $(XDAQ_ROOT)/build/mfRPM.rules