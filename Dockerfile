FROM gitlab-registry.cern.ch/cms-cactus/core/swatch/xdaq15-swatch14:tag-v1.6.10

COPY example/cell/test /test
COPY ci_rpms/*.rpm rpms
RUN yum install -y rpms/*.rpm

CMD [ "/test/run.sh" ]
