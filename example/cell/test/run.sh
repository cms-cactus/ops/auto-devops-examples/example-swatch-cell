#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
cd `dirname "${BASH_SOURCE[0]:-$0}"`/..

export EXAMPLECELL_ROOT=`pwd`
export EXAMPLECELL_TEST=${EXAMPLECELL_ROOT}/test

export SUBSYSTEM_ID=example
export SUBSYSTEM_CELL_CLASS=example::cell::Cell
export SUBSYSTEM_CELL_LIB_PATH="/${EXAMPLECELL_ROOT}/lib/linux/x86_64_centos7/libexampleCell.so"
if [[ ! -f "$SUBSYSTEM_CELL_LIB_PATH" && -f "/opt/xdaq/lib/libexampleCell.so" ]]; then
  export SUBSYSTEM_CELL_LIB_PATH=/opt/xdaq/lib/libexampleCell.so
fi

export SWATCH_DEFAULT_INIT_FILE='${EXAMPLECELL_TEST}/init.xml'
export SWATCH_DEFAULT_GATEKEEPER_XML='${EXAMPLECELL_TEST}/gatekeeper.xml'
export SWATCH_DEFAULT_GATEKEEPER_KEY='Default'

export LD_LIBRARY_PATH=${EXAMPLECELL_ROOT}/lib/linux/x86_64_centos7:${LD_LIBRARY_PATH}

# visit me at http://localhost:3333/urn:xdaq-application:lid=13
/opt/cactus/bin/swatchcell/runSubsystemCell.sh