#include "example/cell/Cell.h"

#include "ts/framework/CellPanelFactory.h"
#include "swatchcell/framework/RedirectPanel.h"

XDAQ_INSTANTIATOR_IMPL(example::cell::Cell)

example::cell::Cell::Cell(xdaq::ApplicationStub *s) : swatchcellframework::CellAbstract(s)
{
}

example::cell::Cell::~Cell()
{
}

void example::cell::Cell::init()
{
  addGenericSwatchComponents();

  tsframework::CellPanelFactory *lPanelFactory = getContext()->getPanelFactory();
  lPanelFactory->add<swatchcellframework::RedirectPanel>("Home");
}
