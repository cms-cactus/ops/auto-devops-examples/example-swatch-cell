#include "config/version.h"
#include "example/cell/version.h"

GETPACKAGEINFO(example::cell)

void example::cell::checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
}

std::set<std::string, std::less<std::string>>
example::cell::getPackageDependencies()
{
  std::set<std::string, std::less<std::string>> dependencies;
  ADDDEPENDENCY(dependencies, config);
  return dependencies;
}
