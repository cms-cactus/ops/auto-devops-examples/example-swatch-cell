#ifndef _EXAMPLE_CELL_CELL_H_
#define _EXAMPLE_CELL_CELL_H_

#include "swatchcell/framework/CellAbstract.h"

namespace example
{
    namespace cell
    {

        class Cell : public swatchcellframework::CellAbstract
        {
        public:
            XDAQ_INSTANTIATOR();

            Cell(xdaq::ApplicationStub *s);

            ~Cell();

            void init();

        private:
            Cell(const Cell &);
        };

    } // namespace cell
} // namespace example

#endif /* _EXAMPLE_CELL_CELL_H_ */
