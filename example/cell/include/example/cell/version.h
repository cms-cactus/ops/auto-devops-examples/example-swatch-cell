#ifndef _example_cell_version_h_
#define _example_cell_version_h_

#include "config/PackageInfo.h"


#define EXAMPLE_CELL_VERSION_MAJOR 1
#define EXAMPLE_CELL_VERSION_MINOR 0
#define EXAMPLE_CELL_VERSION_PATCH 0
// If any previous versions available E.g. #define EXAMPLE_CELL_PREVIOUS_VERSIONS "3.8.0,3.8.1"
//#define EXAMPLE_CELL_PREVIOUS_VERSIONS "0.10"

//
// Template macros
//
#define EXAMPLE_CELL_VERSION_CODE PACKAGE_VERSION_CODE(EXAMPLE_CELL_VERSION_MAJOR,EXAMPLE_CELL_VERSION_MINOR,EXAMPLE_CELL_VERSION_PATCH)
#ifndef EXAMPLE_CELL_PREVIOUS_VERSIONS
  #define EXAMPLE_CELL_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(EXAMPLE_CELL_VERSION_MAJOR,EXAMPLE_CELL_VERSION_MINOR,EXAMPLE_CELL_VERSION_PATCH)
#else
  #define EXAMPLE_CELL_FULL_VERSION_LIST  EXAMPLE_CELL_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(EXAMPLE_CELL_VERSION_MAJOR,EXAMPLE_CELL_VERSION_MINOR,EXAMPLE_CELL_VERSION_PATCH)
#endif
	
namespace example
{
namespace cell
{
  const std::string project = "example";
  const std::string package  = "cell";
  const std::string versions = EXAMPLE_CELL_FULL_VERSION_LIST;
  const std::string description = "Trigger Supervisor Cell for Layer 2 of the Calorimeter Trigger";
  const std::string authors = "Alessandro Thea and Tom Williams";
  const std::string summary = "Trigger Supervisor Cell for Layer 2 of the Calorimeter Trigger";
  const std::string link = "http://cactus.web.cern.ch";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}
}

#endif
